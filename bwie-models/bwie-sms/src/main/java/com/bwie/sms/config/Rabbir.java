package com.bwie.sms.config;

import com.alibaba.fastjson.JSON;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.utils.TelSmsUtils;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Component
public class Rabbir {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @RabbitListener(queuesToDeclare = {@Queue("sends")})
    public void rabbit(String msg, Channel channel, Message message) {

        String messageId = message.getMessageProperties().getMessageId();

        Long sends = redisTemplate.opsForSet().add("sends", messageId);

        if (sends==1){
            UserResponse userResponse = JSON.parseObject(msg, UserResponse.class);

            TelSmsUtils.sendSms(userResponse.getUserPhone(),"10001",new HashMap<String, String>(){{
                put("code","你有一条消息");
            }});

            System.out.println("------发送成功---");
        }

        try {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
