package com.bwie.es.feign;

import com.bwie.common.domain.response.Shop;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "bwie-product")
public interface ShopServiceFegin {

    @GetMapping("/esList")
    public Result<List<Shop>> result ();


}
