package com.bwie.es.service;

import com.bwie.common.domain.request.ShopReq;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.result.Result;

import java.util.List;

public interface EsService {
    Result<List<Shop>> esList(ShopReq shopReq);

    Result esAll();

    Result esRabbit(Shop shop);

}
