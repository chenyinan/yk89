package com.bwie.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class esApplication {
    public static void main(String[] args) {
        SpringApplication.run(esApplication.class);
    }
}
