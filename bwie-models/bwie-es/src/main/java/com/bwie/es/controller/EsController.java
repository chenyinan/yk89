package com.bwie.es.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.bwie.common.domain.request.ShopAdd;
import com.bwie.common.domain.request.ShopReq;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.result.Result;
import com.bwie.es.service.EsService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Log4j2
public class EsController {

    @Autowired
    private EsService esService;

    @Autowired
    private HttpServletRequest request;



    @PostMapping("/esFind")
    public Result<List<Shop>> shop(@RequestBody ShopReq shopReq){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());

        Result<List<Shop>> responseResult=esService.esList(shopReq);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @PostMapping("/esAll")
    public Result esAll(){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());

        Result responseResult=esService.esAll();
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }


    @PostMapping("/esRabbit")
    public Result esRabbit(@RequestBody Shop shop){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());

        Result responseResult=esService.esRabbit(shop);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }


}
