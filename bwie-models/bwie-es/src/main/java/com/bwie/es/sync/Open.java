package com.bwie.es.sync;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.result.Result;
import com.bwie.es.feign.ShopServiceFegin;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Component
public class Open implements ApplicationRunner {

    @Autowired
    private ShopServiceFegin shopServiceFegin;

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Result<List<Shop>> result = shopServiceFegin.result();

        List<Shop> data = result.getData();

        IndexRequest indexRequest = new IndexRequest("shopalls");


        data.forEach(l->{

            //根据当前时间和过期时间来判断是不是在30天内


            indexRequest.id(l.getShopId()+"");
            IndexRequest source = indexRequest.source(JSON.toJSONString(l), XContentType.JSON);
            try {
                restHighLevelClient.index(source, RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        System.out.println("-----------------------------同步成功-----------------------------");

    }
}
