package com.bwie.es.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.bwie.common.domain.request.ShopReq;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.result.Result;
import com.bwie.common.utils.StringUtils;
import com.bwie.es.feign.ShopServiceFegin;
import com.bwie.es.service.EsService;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EsServiceImpl implements EsService {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Autowired
    private ShopServiceFegin shopServiceFegin;

    @Override
    public Result<List<Shop>> esList(ShopReq shopReq) {

        ArrayList<Shop> shops = new ArrayList<>();

        SearchRequest shopalls = new SearchRequest("shopalls");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        if (StringUtils.isNotEmpty(shopReq.getShopName())){
            boolQueryBuilder.must(QueryBuilders.matchQuery("shopName",shopReq.getShopName()));
        }


        if (shopReq.getShopTime()!=null){
            boolQueryBuilder.must(QueryBuilders.rangeQuery("shopTime").lte(shopReq.getShopTime().getTime()));
        }

     if (shopReq.getShopTime1()!=null){
            boolQueryBuilder.must(QueryBuilders.rangeQuery("shopTime").gte(shopReq.getShopTime1().getTime()));
        }


     searchSourceBuilder.query(boolQueryBuilder);


        HighlightBuilder highlightBuilder = new HighlightBuilder();

        highlightBuilder.field("shopName");
        highlightBuilder.preTags("<font color=\"red\">");
        highlightBuilder.postTags("</font>");

        searchSourceBuilder.highlighter(highlightBuilder);

        shopalls.source(searchSourceBuilder);

        try {
            SearchResponse search = restHighLevelClient.search(shopalls, RequestOptions.DEFAULT);


            SearchHits hits = search.getHits();


            SearchHit[] hits1 = hits.getHits();
            for (SearchHit hit : hits1) {

                String sourceAsString = hit.getSourceAsString();
                Shop shop = JSON.parseObject(sourceAsString, Shop.class);
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();

                if (highlightFields!=null){
                    HighlightField shopName = highlightFields.get("shopName");
                    if (shopName!=null){
                        Text[] fragments = shopName.getFragments();

                       String as="";

                        for (Text fragment : fragments) {
                            as+=fragment;
                        }

                        shop.setShopName(as);
                    }

                }
                //根据当前时间和过期时间来判断是不是在30天内
                long between = DateUtil.between(new Date(), shop.getShopTimeEnd(), DateUnit.DAY);


                if (between<30){
                    shop.setStart(5);
                }



                shops.add(shop);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


        return Result.success(shops);
    }

    @Override
    public Result esAll() {
        Result<List<Shop>> result = shopServiceFegin.result();

        List<Shop> data = result.getData();

        IndexRequest indexRequest = new IndexRequest("shopalls");


        data.forEach(l->{
            indexRequest.id(l.getShopId()+"");
            IndexRequest source = indexRequest.source(JSON.toJSONString(l), XContentType.JSON);
            try {
                restHighLevelClient.index(source, RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return Result.success("同步成功");
    }

    @Override
    public Result esRabbit(Shop shop) {


        return null;
    }
}
