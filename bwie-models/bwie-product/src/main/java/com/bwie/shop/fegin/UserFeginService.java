package com.bwie.shop.fegin;

import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "bwie-auth")
public interface UserFeginService {
    @PostMapping("/rabbit")
    public Result rabbit(@RequestBody UserResponse userResponse);

    @PostMapping("/EsAlls")
    public Result EsAlls();
}
