package com.bwie.shop.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.naming.pojo.healthcheck.impl.Http;
import com.bwie.common.constants.JwtConstants;
import com.bwie.common.domain.request.ShopAdd;
import com.bwie.common.domain.request.Upda;
import com.bwie.common.domain.response.Count;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import com.bwie.common.utils.JwtUtils;
import com.bwie.shop.dao.ShopDao;
import com.bwie.shop.fegin.EsFeginService;
import com.bwie.shop.fegin.UserFeginService;
import com.bwie.shop.service.ShopService;
import org.apache.http.client.utils.DateUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private EsFeginService esFeginService;

    @Autowired
    private UserFeginService userFeginService;

  @Autowired
    private ShopDao shopDao;

  @Autowired
  private RabbitTemplate rabbitTemplate;

  @Autowired
  private HttpServletRequest request;
  @Autowired
  private RedisTemplate<String,String> redisTemplate;

    @Override
    public Result<List<Shop>> esList() {
        List<Shop> list=shopDao.list();
        List<Count> counts=shopDao.count();
//双集合循环判断赋值

        list.forEach(l->{
            counts.forEach(ll->{
                if (l.getShopUid().equals(ll.getShopUid())){
                    l.setCount(ll.getCount());
                }
            });
        });

        return Result.success(list);
    }

    @Override
    public Result insert(ShopAdd shopAdd) {
        //获取当前登录人的消息
        String token = request.getHeader("token");

        String userKey = JwtUtils.getUserKey(token);

        String s = redisTemplate.opsForValue().get(JwtConstants.USER_KEY + userKey);


        UserResponse response = JSON.parseObject(s, UserResponse.class);

        //租借时间必须在30天以上
        long between = DateUtil.between(shopAdd.getShopTimeBegin(), shopAdd.getShopTimeEnd(), DateUnit.DAY);

        if (between<=30){
            return Result.error("租借天数必须在30天以上");
        }


        shopAdd.setShopUid(response.getUserId());

        shopAdd.setShopTime(new Date());
        shopDao.insert(shopAdd);

        //远程调用es接口 实现同步
        Result result = esFeginService.esAll();
     /*   Result result1 = userFeginService.EsAlls();*/

        return Result.success("添加成功");
    }

    @Override
    public Result upda(Upda upda) {

        shopDao.upda(upda);
        //远程调用es接口 实现同步
        Result result = esFeginService.esAll();

        //根据id查找对应用户
       UserResponse userResponse= shopDao.find(upda.getShopUid());

      /*  Result result1 = userFeginService.EsAlls();*/

        userFeginService.rabbit(userResponse);

        return Result.success("修改成功");
    }

    @Override
    public Result<List<Shop>> find(Integer uid) {
        List<Shop> shops=shopDao.shopFind(uid);
        return Result.success(shops);
    }

    @Override
    public Result money(Integer money) {

        Integer moneys=money-500;
        if (moneys>0||moneys<0){
            return Result.error("保证金输入错误");
        }else {
            return Result.success("保证金输入成功");
        }
    }


}
