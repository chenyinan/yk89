package com.bwie.shop.service;

import com.bwie.common.domain.request.ShopAdd;
import com.bwie.common.domain.request.Upda;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.result.Result;

import java.util.List;

public interface ShopService {
    Result<List<Shop>> esList();

    Result insert(ShopAdd shopAdd);

    Result upda(Upda upda);


    Result<List<Shop>> find(Integer uid);


    Result money(Integer money);

}

