package com.bwie.shop.controller;

import com.alibaba.fastjson.JSON;
import com.bwie.common.domain.request.ShopAdd;
import com.bwie.common.domain.request.Upda;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import com.bwie.shop.service.ShopService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Log4j2
public class ShopController {


    @Autowired
    private ShopService shopService;

    @Autowired
    private HttpServletRequest request;

    //查询所有信息
    @GetMapping("/esList")
    public Result<List<Shop>> result (){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());

        Result<List<Shop>> responseResult=shopService.esList();
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }


    //添加
    @PostMapping("/insert")
    public Result insert(@RequestBody ShopAdd shopAdd){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(),JSON.toJSONString(shopAdd));

        Result responseResult=shopService.insert(shopAdd);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }


    //管路员审核
    @PostMapping("/upda")
    public Result upda(@RequestBody Upda upda){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(),JSON.toJSONString(upda));

        Result responseResult=shopService.upda(upda);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @PostMapping("/find/{uid}")
    public Result<List<Shop>> find(@PathVariable Integer uid){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(),JSON.toJSONString(uid));

        Result<List<Shop>> responseResult=shopService.find(uid);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @PostMapping("/money/{money}")
    public Result money(@PathVariable Integer money){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(),JSON.toJSONString(money));

        Result responseResult=shopService.money(money);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }




}
