package com.bwie.shop.fegin;

import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "bwie-es")
public interface EsFeginService {
    @PostMapping("/esAll")
    public Result esAll();
}
