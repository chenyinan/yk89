package com.bwie.shop.dao;

import com.bwie.common.domain.request.ShopAdd;
import com.bwie.common.domain.request.Upda;
import com.bwie.common.domain.response.Count;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.domain.response.UserResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShopDao {
    List<Shop> list();

    List<Count> count();

    void insert(ShopAdd shopAdd);


    void upda(Upda upda);

    UserResponse find(@Param("shopUid") Integer shopUid);

    List<Shop> shopFind(@Param("uid") Integer uid);

}
