package com.bwie.user.service;

import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;

public interface UserService {
    Result<UserResponse> findName(String phone);

}
