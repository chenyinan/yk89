package com.bwie.user.mapper;

import com.bwie.common.domain.response.UserResponse;
import org.apache.ibatis.annotations.Param;

public interface UserDao {
    UserResponse findName(@Param("phone") String phone);

}
