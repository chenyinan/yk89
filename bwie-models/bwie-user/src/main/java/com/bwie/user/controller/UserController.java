package com.bwie.user.controller;

import com.alibaba.fastjson.JSON;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import com.bwie.user.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Log4j2
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest request;

  @PostMapping("/findPhone/{phone}")
    public Result<UserResponse> result(@PathVariable String phone){
      log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(phone));

      Result<UserResponse> responseResult=userService.findName(phone);
      log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
      return responseResult;
  }
}
