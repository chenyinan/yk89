package com.bwie.user.service.impl;

import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import com.bwie.user.mapper.UserDao;
import com.bwie.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public Result<UserResponse> findName(String phone) {
       UserResponse response= userDao.findName(phone);
        return Result.success(response);
    }
}
