package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class Token {
    private String token;
    private String time;
}

