package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Phone {
    private String phone;
    private String code;
}
