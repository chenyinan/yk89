package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class Count {
    private Integer count;
    private Integer shopUid;
}
