package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class UserResponse {
    private Integer userId;
    private String userPhone;
    private String userName;
    private Integer start;



}
