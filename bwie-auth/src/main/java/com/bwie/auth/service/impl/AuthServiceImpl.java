package com.bwie.auth.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.bwie.auth.feign.UserServiceFegin;
import com.bwie.auth.service.AuthService;
import com.bwie.common.constants.JwtConstants;
import com.bwie.common.domain.request.Phone;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.domain.response.Token;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import com.bwie.common.utils.JwtUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private UserServiceFegin userServiceFegin;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;


    @Override
    public Result code(String phone) {
        Result<UserResponse> result = userServiceFegin.result(phone);


        UserResponse data = result.getData();

        if (data==null){
            return Result.error("不存在此手机号");
        }

        String s = RandomUtil.randomNumbers(4);

        redisTemplate.opsForValue().set(phone,s,3, TimeUnit.MINUTES);

        return Result.success(s);
    }

    @Override
    public Result<Token> login(Phone phone) {
        Result<UserResponse> result = userServiceFegin.result(phone.getPhone());

        UserResponse data = result.getData();

        if (data==null){
            return Result.error("不存在此手机号");
        }

        String s = redisTemplate.opsForValue().get(phone.getPhone());


        if (!phone.getCode().equals(s)){
            return Result.error("验证码错误");
        }

        String userkey = UUID.randomUUID().toString().replaceAll("-", "");

        HashMap<String, Object> map = new HashMap<>();

        map.put(JwtConstants.USER_KEY,userkey);

        String token = JwtUtils.createToken(map);


        redisTemplate.opsForValue().set(JwtConstants.USER_KEY+userkey, JSON.toJSONString(data),20,TimeUnit.MINUTES);

        Token token1 = new Token();
        token1.setToken(token);
        token1.setTime("20");

        return Result.success(token1);
    }

    @Override
    public Result<UserResponse> info() {
        String token = request.getHeader("token");

        String userKey = JwtUtils.getUserKey(token);

        String s = redisTemplate.opsForValue().get(JwtConstants.USER_KEY + userKey);


        UserResponse response = JSON.parseObject(s, UserResponse.class);


        return Result.success(response);
    }

    @Override
    public Result logout() {
        String token = request.getHeader("token");

        String userKey = JwtUtils.getUserKey(token);

        redisTemplate.delete(JwtConstants.USER_KEY+userKey);
        return Result.error("退出成功");
    }

    @Override
    public Result rabbit(UserResponse userResponse) {
        rabbitTemplate.convertAndSend("sends",JSON.toJSONString(userResponse),msg->{
            msg.getMessageProperties().setMessageId(UUID.randomUUID().toString());
            return msg;
        });

        return Result.success("发送成功");
    }

    @Override
    public Result rabbitEs() {
        rabbitTemplate.convertAndSend("rabbitEs",JSON.toJSONString("as"),msg->{
            msg.getMessageProperties().setMessageId(UUID.randomUUID().toString());
            return msg;
        });
        return null;
    }


}
