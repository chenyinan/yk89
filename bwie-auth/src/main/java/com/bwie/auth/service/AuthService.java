package com.bwie.auth.service;

import com.bwie.common.domain.request.Phone;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.domain.response.Token;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;

public interface AuthService {
    Result code(String phone);

    Result<Token> login(Phone phone);

    Result<UserResponse> info();

    Result logout();

    Result rabbit(UserResponse userResponse);


    Result rabbitEs();

}
