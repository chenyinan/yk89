package com.bwie.auth.controller;

import com.alibaba.fastjson.JSON;
import com.bwie.auth.service.AuthService;
import com.bwie.common.domain.request.Phone;
import com.bwie.common.domain.response.Shop;
import com.bwie.common.domain.response.Token;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Log4j2
public class AuthController {
    @Autowired
    private AuthService authService;

    @Autowired
    private HttpServletRequest request;


    @PostMapping("/user/code/{phone}")
    public Result result(@PathVariable String phone){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(phone));

        Result responseResult=authService.code(phone);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @PostMapping("/login")
    public Result<Token> login(@RequestBody Phone phone){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(phone));

        Result<Token> responseResult=authService.login(phone);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @GetMapping("/user/info")
    public Result<UserResponse> info(){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());

        Result<UserResponse> responseResult=authService.info();
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @PostMapping("/user/logout")
    public Result logout(){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result responseResult=authService.logout();
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @PostMapping("/rabbit")
    public Result rabbit(@RequestBody UserResponse userResponse){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result responseResult=authService.rabbit(userResponse);
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }

    @PostMapping("/EsAlls")
    public Result EsAlls(){
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result responseResult=authService.rabbitEs();
        log.info("访问名称：查找，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(responseResult));
        return responseResult;
    }


}
