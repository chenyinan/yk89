package com.bwie.auth.feign;

import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "bwie-user")
public interface UserServiceFegin {

    @PostMapping("/findPhone/{phone}")
    public Result<UserResponse> result(@PathVariable String phone);
}
